<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:110:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/teacher/edit.html";i:1560143950;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1560143951;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="/static/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="/static/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="/static/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="/static/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="/static/css/paging.css" rel="stylesheet" />
		
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				

<div class="header">
	<h1 class="page-header">
		老师管理
    			<small>账号管理</small>
                       </h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li>
			<a href="<?php echo url('teacher/index'); ?>">老师管理</a>
		</li>
		<li class="active">账号管理</li>
	</ol>

</div>
<div id="page-inner">
	<div class="row">
		<form class="col-md-12" method="post">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="card-title">
						<div class="title">老师管理</div>
					</div>
				</div>
				<input type="hidden" name="id" value="<?php echo $teacher['id']; ?>" />
				<div class="panel-body">
					<div class="form-horizontal" style="margin-bottom: 200px;">
						<label class="col-sm-2 control-label">头像</label>
						<div class="col-sm-10">
							<div id="container">
								<a class="btn btn-default btn-lg " id="pickfiles" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>选择图片</span>
								</a> jpg格式
							</div><br />
							<div>
								<div class="col-sm-4">
									<div id="img">
										<img class="img-responsive" width="100" height="100" src="<?php echo $teacher['avatar']; ?>" />
									</div>
									<input id="cover" type="hidden" name="avatar" value="<?php echo $teacher['avatar']; ?>" />
									<div id="img-progress">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-horizontal" style="margin-bottom: 200px;">
						<label class="col-sm-2 control-label">介绍语音</label>
						<div class="col-sm-10">
							<div id="container-mp3">
								<a class="btn btn-default btn-lg " id="pickfiles-mp3" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>选择音频</span>
								</a> MP3格式
							</div><br />
							<div>
								<div class="col-sm-4">
									<div id="mp3">
										<?php if($teacher['mp3'] != null): ?>
										<audio src="<?php echo $teacher['mp3']; ?>" controls="controls"></audio>
										<?php endif; ?>
									</div>
									<input id="divmp3" type="hidden" value="<?php echo $teacher['mp3']; ?>" name="mp3" />
									<div id="mp3-progress">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-horizontal" style="margin-bottom: 200px;">
						<label class="col-sm-2 control-label">介绍视频</label>
						<div class="col-sm-10">
							<div id="container-mp4">
								<a class="btn btn-default btn-lg " id="pickfiles-mp4" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>选择视频</span>
								</a> MP4格式
							</div><br />
							<div>
								<div class="col-sm-4">
									<div id="mp4">
										<?php if($teacher['mp4'] != null): ?>
										<video src="<?php echo $teacher['mp4']; ?>" width="320" controls="controls"></video> 
										<?php endif; ?>
									</div>
									<input id="divmp4" type="hidden" value="<?php echo $teacher['mp4']; ?>" name="mp4" />
									<div id="mp4-progress">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">老师昵称</label>
							<div class="col-sm-4">
								<input type="text" name="nickName" value="<?php echo $teacher['nickName']; ?>" class="form-control" required="required">
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">英文名</label>
							<div class="col-sm-4">
								<input type="text" name="name_en" value="<?php echo $teacher['name_en']; ?>" class="form-control" required="required">
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">微信号</label>
							<div class="col-sm-4">
								<input type="text" name="wx" value="<?php echo $teacher['wx']; ?>" class="form-control" required="required">
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">手机号</label>
							<div class="col-sm-4">
								<input type="text" name="phone" value="<?php echo $teacher['phone']; ?>" class="form-control" required="required">
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">登录账号</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" value="<?php echo $teacher['account']; ?>" disabled>
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">登录密码</label>
							<div class="col-sm-4">
								<input type="text" name="password" class="form-control" placeholder="不修改请不要填写">
								<p class="help-block">不修改请不要填写此项。</p>
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">账号状态</label>
							<div class="col-sm-4">
								<select name="state" class="form-control">
									<?php if($teacher['state'] == 1): ?>
									<option value="1" selected="selected">已开启</option>
									<option value="0">已关闭</option>
									<?php else: ?>
									<option value="1">已开启</option>
									<option value="0" selected="selected">已关闭</option>
									<?php endif; ?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">自我介绍</label>
							<div class="col-sm-4">
								<textarea class="form-control" name="introduction" rows="5"><?php echo $teacher['introduction']; ?></textarea>
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">英文介绍</label>
							<div class="col-sm-4">
								<textarea class="form-control" name="introduction_en" rows="5"><?php echo $teacher['introduction_en']; ?></textarea>
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">老师标签</label>
							<div class="col-sm-10">
								<?php if(is_array($taglist) || $taglist instanceof \think\Collection || $taglist instanceof \think\Paginator): $i = 0; $__LIST__ = $taglist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<label class="checkbox-inline">
								<?php if(in_array(($vo['id']), is_array($teacher['tag'])?$teacher['tag']:explode(',',$teacher['tag']))): ?>
									<input type="checkbox" name="tag[]" value="'<?php echo $vo['id']; ?>'" checked="checked"> <?php echo $vo['name_cn']; else: ?>
									<input type="checkbox" name="tag[]" value="'<?php echo $vo['id']; ?>'"> <?php echo $vo['name_cn']; endif; ?>
								</label>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">姓名</label>
							<div class="col-sm-4">
								<input type="text" name="name" value="<?php echo $teacher['name']; ?>" class="form-control" required="required">
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">结算银行</label>
							<div class="col-sm-4">
								<input type="text" name="bank" value="<?php echo $teacher['bank']; ?>" class="form-control" required="required">
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">结算账号</label>
							<div class="col-sm-4">
								<input type="text" name="bank_account" value="<?php echo $teacher['bank_account']; ?>" class="form-control" required="required">
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">身份证正面</label>
							<div class="col-sm-4">
								<img src="<?php echo $teacher['identification1']; ?>" class="img-responsive">
							</div>
						</div>
					</div>
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">身份证背面</label>
							<div class="col-sm-4">
								<img src="<?php echo $teacher['identification2']; ?>" class="img-responsive">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-body">
					<button type="submit" class="btn btn-danger pull-right">保存资料</button>
				</div>
			</div>
		</form>
	</div>
</div>

			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="/static/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="/static/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="/static/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="/static/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="/static/js/paging.js"></script>
		
<script src="/static/Plupload/moxie.js"></script>
<script src="/static/Plupload/plupload.dev.js"></script>
<script src="/static/Plupload/i18n/zh_CN.js"></script>
<script src="/static/js/qiniu.min.js"></script>
<script>
	var domain = "http://img.panpanchinese.cn/";
	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		mime_types: [{
			title: "Image files",
			extensions: "jpg"
		}],
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#img-progress').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("img").innerHTML =
					'<img id="img" class="img-responsive" width="100" height="100" src="' + domain + res.key + '?imageView2/1/w/200/h/200/interlace/1" />';
				$('#cover').val(domain + res.key + '?imageView2/1/w/200/h/200/interlace/1');
				$('#img-progress').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});
	
	
	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles-mp3', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container-mp3', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container-mp3', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#mp3-progress').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("mp3").innerHTML =
				'<audio src="' + domain + res.key + '" controls="controls"></audio>';
				$('#divmp3').val(domain + res.key);
				$('#mp3-progress').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});	
	
	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles-mp4', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container-mp4', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container-mp4', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#mp4-progress').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("mp4").innerHTML =
				'<video src="' + domain + res.key + '" width="320" controls="controls"></video>';
				$('#divmp4').val(domain + res.key);
				$('#mp4-progress').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>