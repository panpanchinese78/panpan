<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:110:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/course/video.html";i:1560496025;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1560143951;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="__STATIC__/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="__STATIC__/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="__STATIC__/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="__STATIC__/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="__STATIC__/css/paging.css" rel="stylesheet" />
		
<link href="__STATIC__/css/bootstrap-datetimepicker.min.css" rel="stylesheet" /> 
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				
<div class="header">
	<h1 class="page-header">
		教材管理
    		<small>视频管理</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li>
			<a href="<?php echo url('course/index'); ?>">教材管理</a>
		</li>
		<li class="active">视频管理</li>
	</ol>

</div>
<div id="page-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- Advanced Tables -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-12">
                           ===
						</div>
					</div>
					<!-- /input-group -->
				</div>
				<div class="panel-body">
					<div class="table-responsive">
                        <?php var_dump($video);  ?>
                        <ul>
							<?php if(is_array($video) || $video instanceof \think\Collection || $video instanceof \think\Paginator): if( count($video)==0 ) : echo "" ;else: foreach($video as $k=>$vo): ?>
							<li><?php echo $k; ?>|<?php echo $vo; ?></li>
							<?php endforeach; endif; else: echo "" ;endif; ?>
						</ul>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="box" id="box"></div>
						</div>
					</div>
				</div>
			</div>
			<!--End Advanced Tables -->
		</div>
	</div>
</div>


			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="__STATIC__/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="__STATIC__/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="__STATIC__/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="__STATIC__/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="__STATIC__/js/paging.js"></script>
		
<script src="__STATIC__/js/bootstrap-datetimepicker.min.js"></script>
<script src="__STATIC__/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="__STATIC__/Plupload/moxie.js"></script>
<script src="__STATIC__/Plupload/plupload.dev.js"></script>
<script src="__STATIC__/Plupload/i18n/zh_CN.js"></script>
<script src="__STATIC__/js/qiniu.min.js"></script>
<script>
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>