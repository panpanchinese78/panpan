<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:112:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/timezone/index.html";i:1560143953;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1560143951;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="__STATIC__/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="__STATIC__/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="__STATIC__/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="__STATIC__/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="__STATIC__/css/paging.css" rel="stylesheet" />
		
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				

<div class="header">
	<h1 class="page-header">
		时区管理
	<?php switch(\think\Request::instance()->param('state')): case "1": ?><small>可使用</small><?php break; case "0": ?><small>已关闭</small><?php break; default: ?>
    		<small>全部时区</small>
	<?php endswitch; ?>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li>
			<a href="<?php echo url('timezone/index'); ?>">时区管理</a>
		</li>
		<?php switch(\think\Request::instance()->param('state')): case "1": ?>
		<li class="active">可使用</li><?php break; case "0": ?>
		<li class="active">已关闭</li><?php break; default: ?>
		<li class="active">全部时区</li>
		<?php endswitch; ?>
	</ol>
 
</div>
<div id="page-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			<?php switch(\think\Request::instance()->param('state')): case "1": ?>
				<li><a href="<?php echo url('timezone/index'); ?>">全部时区</a></li>
				<li class="active"><a href="<?php echo url('timezone/index',['state'=>1]); ?>">可使用</a></li>
				<li><a href="<?php echo url('timezone/index',['state'=>0]); ?>">已关闭</a></li>
				<?php break; case "0": ?>
				<li><a href="<?php echo url('timezone/index'); ?>">全部时区</a></li>
				<li><a href="<?php echo url('timezone/index',['state'=>1]); ?>">可使用</a></li>
				<li class="active"><a href="<?php echo url('timezone/index',['state'=>0]); ?>">已关闭</a></li>
				<?php break; default: ?>
				<li class="active"><a href="<?php echo url('timezone/index'); ?>">全部时区</a></li>
				<li><a href="<?php echo url('timezone/index',['state'=>1]); ?>">可使用</a></li>
				<li><a href="<?php echo url('timezone/index',['state'=>0]); ?>">已关闭</a></li>
			<?php endswitch; ?>
			</ul>
			<!-- Advanced Tables -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-12"> 
							<button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#addTimezone">新增时区</button>
						</div>
					</div>
					<!-- /input-group -->
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>时区名称（中文）</th>
									<th>时区名称（英文）</th>
									<th>时区名称（韩文）</th>
									<th>时区时间</th>
									<th>时区状态</th>
									<th width="100">时区管理</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<tr>
									<td class="center"><?php echo $vo['name_cn']; ?></td>
									<td class="center"><?php echo $vo['name_en']; ?></td>
									<td class="center"><?php echo $vo['name_ko']; ?></td>
									<td class="center"><?php echo $vo['time_zone_name']; ?></td>
									<td class="center"><?php if($vo['state'] == 0): ?>已关闭<?php else: ?>可使用<?php endif; ?></td>
									<td class="center">
										<?php if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
										<div class="btn-group" role="group" aria-label="...">
											<button type="button" onclick="editTimezone('<?php echo $vo['id']; ?>','<?php echo $vo['name_cn']; ?>','<?php echo $vo['name_ko']; ?>','<?php echo $vo['name_en']; ?>','<?php echo $vo['time_zone']; ?>','<?php echo $vo['state']; ?>')" class="btn btn-danger">时区管理</button>
										</div>
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="box" id="box"></div>
						</div>
					</div>
				</div>
			</div>
			<!--End Advanced Tables -->
		</div>
	</div>
</div>
<div id="addTimezone" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">新增时区</h4>
      </div>
      <div class="modal-body">
		<div class="form-group">
      	<select id="addTimezoneT" class="form-control">
      		<?php if(is_array($TimezoneList) || $TimezoneList instanceof \think\Collection || $TimezoneList instanceof \think\Paginator): $i = 0; $__LIST__ = $TimezoneList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tl): $mod = ($i % 2 );++$i;?>
			<option value="<?php echo $tl['id']; ?>"><?php echo $tl['name']; ?></option>
			<?php endforeach; endif; else: echo "" ;endif; ?>
		</select>
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="addTimezoneCn" placeholder="时区名称（中文）">
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="addTimezoneKo" placeholder="时区名称（韩文）">
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="addTimezoneEn" placeholder="时区名称（英文）">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" onclick="addTimezone()">保存</button> 
      </div>
    </div>
  </div>
</div>
<div id="editTimezone" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">时区管理</h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" id="editTimezoneId" />
		<div class="form-group">
      	<select id="editTimezoneT" class="form-control">
      		<?php if(is_array($TimezoneList) || $TimezoneList instanceof \think\Collection || $TimezoneList instanceof \think\Paginator): $i = 0; $__LIST__ = $TimezoneList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$tl): $mod = ($i % 2 );++$i;?>
			<option value="<?php echo $tl['id']; ?>"><?php echo $tl['name']; ?></option>
			<?php endforeach; endif; else: echo "" ;endif; ?>
		</select>
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="editTimezoneCn" placeholder="时区名称（中文）">
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="editTimezoneKo" placeholder="时区名称（韩文）">
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="editTimezoneEn" placeholder="时区名称（英文）">
		</div>
		<div class="form-group">
      	<select id="editTimezoneState" class="form-control">
			<option value="0">已关闭</option>
			<option value="1">可使用</option>
		</select>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" onclick="update()">保存</button> 
      </div>
    </div>
  </div>
</div>

			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="__STATIC__/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="__STATIC__/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="__STATIC__/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="__STATIC__/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="__STATIC__/js/paging.js"></script>
		
<script>
	var setTotalCount = <?php echo $total; ?>;
	var p = <?php echo $p; ?>;
	$('#box').paging({
		initPageNo: <?php echo $p; ?>, // 初始页码
		totalPages: <?php echo $pageNum; ?>, //总页数
		totalCount: '共' + setTotalCount + '条数据', // 条目总数
		slideSpeed: 300, // 缓动速度。单位毫秒
		jump: false, //是否支持跳转
		callback: function(page) { // 回调函数
			if(page != p) {
				window.location.replace("<?php echo url('timezone/index'); ?>" + '?p=' + page + '&state=<?php echo $state; ?>');
			}
		}
	})
</script>
<script>
	//新增时区
	function addTimezone() {
		var time_zone = $('#addTimezoneT').val();
		var cn = $('#addTimezoneCn').val();
		var ko = $('#addTimezoneKo').val();
		var en = $('#addTimezoneEn').val();
		if(cn.length < 1||ko.length < 1||en.length < 1){
			alert('请正确输入时区名称');
			return false;
		}
		info = '是否新增?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('timezone/add'); ?>",
				data: {
					time_zone: time_zone,
					name_cn: cn,
					name_ko: ko,
					name_en: en
				},
				success: function(result) {
					$('#addTimezone').modal('hide');
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
</script>
<script>
	//更新
	function editTimezone(id,cn,ko,en,timezone,state) {
		$('#editTimezoneId').val(id);
		$("#editTimezoneCn").val(cn);
		$('#editTimezoneKo').val(ko);
		$('#editTimezoneEn').val(en);
		$('#editTimezoneT').val(timezone);
		$("#editTimezoneState").val(state);
		$('#editTimezone').modal('show');
	}
</script>
<script>
	//确认更新
	function update() {
		var id = $('#editTimezoneId').val();
		var 	time_zone = $('#editTimezoneT').val();
		var 	cn = $("#editTimezoneCn").val();
		var 	ko = $('#editTimezoneKo').val();
		var 	en = $('#editTimezoneEn').val();
		var state = $('#editTimezoneState').val();
		if(cn.length < 1||ko.length < 1||en.length < 1){
			alert('请正确输入时区名称');
			return false;
		}
		info = '是否修改?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('timezone/edit'); ?>",
				data: {
					id: id,
					name_cn: cn,
					name_ko: ko,
					name_en: en,
					time_zone: time_zone,
					state: state
				},
				success: function(result) {
					$('#editTimezone').modal('hide');
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>