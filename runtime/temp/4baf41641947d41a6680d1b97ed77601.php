<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:109:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/order/index.html";i:1560143951;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1560143951;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="__STATIC__/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="__STATIC__/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="__STATIC__/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="__STATIC__/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="__STATIC__/css/paging.css" rel="stylesheet" />
		
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				

<div class="header">
	<h1 class="page-header">
		预约管理
	<?php switch(\think\Request::instance()->param('state')): case "0": ?><small>待开始</small><?php break; case "1": ?><small>已完成</small><?php break; case "2": ?><small>已取消</small><?php break; case "3": ?><small>待评价</small><?php break; default: ?>
    		<small>全部</small>
	<?php endswitch; ?>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li>
			<a href="<?php echo url('order/index'); ?>">预约管理</a>
		</li>
		<?php switch(\think\Request::instance()->param('state')): case "0": ?>
		<li class="active">待开始</li><?php break; case "1": ?>
		<li class="active">已完成</li><?php break; case "2": ?>
		<li class="active">已取消</li><?php break; case "3": ?>
		<li class="active">待评价</li><?php break; default: ?>
		<li class="active">全部</li>
		<?php endswitch; ?>
	</ol>
 
</div>
<div id="page-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			<?php switch(\think\Request::instance()->param('state')): case "0": ?>
				<li><a href="<?php echo url('order/index'); ?>">全部</a></li>
				<li class="active"><a href="<?php echo url('order/index',['state'=>0]); ?>">待开始</a></li>
				<li><a href="<?php echo url('order/index',['state'=>1]); ?>">已完成</a></li>
				<li><a href="<?php echo url('order/index',['state'=>2]); ?>">已取消</a></li>
				<li><a href="<?php echo url('order/index',['state'=>3]); ?>">待评价</a></li>
				<?php break; case "1": ?>
				<li><a href="<?php echo url('order/index'); ?>">全部</a></li>
				<li><a href="<?php echo url('order/index',['state'=>0]); ?>">待开始</a></li>
				<li class="active"><a href="<?php echo url('order/index',['state'=>1]); ?>">已完成</a></li>
				<li><a href="<?php echo url('order/index',['state'=>2]); ?>">已取消</a></li>
				<li><a href="<?php echo url('order/index',['state'=>3]); ?>">待评价</a></li>
				<?php break; case "2": ?>
				<li><a href="<?php echo url('order/index'); ?>">全部</a></li>
				<li><a href="<?php echo url('order/index',['state'=>0]); ?>">待开始</a></li>
				<li><a href="<?php echo url('order/index',['state'=>1]); ?>">已完成</a></li>
				<li class="active"><a href="<?php echo url('order/index',['state'=>2]); ?>">已取消</a></li>
				<li><a href="<?php echo url('order/index',['state'=>3]); ?>">待评价</a></li>
				<?php break; case "3": ?>
				<li><a href="<?php echo url('order/index'); ?>">全部</a></li>
				<li><a href="<?php echo url('order/index',['state'=>0]); ?>">待开始</a></li>
				<li><a href="<?php echo url('order/index',['state'=>1]); ?>">已完成</a></li>
				<li><a href="<?php echo url('order/index',['state'=>2]); ?>">已取消</a></li>
				<li class="active"><a href="<?php echo url('order/index',['state'=>3]); ?>">待评价</a></li>
				<?php break; default: ?>
				<li class="active"><a href="<?php echo url('order/index'); ?>">全部</a></li>
				<li><a href="<?php echo url('order/index',['state'=>0]); ?>">待开始</a></li>
				<li><a href="<?php echo url('order/index',['state'=>1]); ?>">已完成</a></li>
				<li><a href="<?php echo url('order/index',['state'=>2]); ?>">已取消</a></li>
				<li><a href="<?php echo url('order/index',['state'=>3]); ?>">待评价</a></li>
			<?php endswitch; ?>
			</ul>
			<!-- Advanced Tables -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-6">
							<form class="input-group" action="<?php echo url('order/index'); ?>" method="get">
								<input type="text" id="search" class="form-control" name="search" placeholder="老师学员账号/昵称/微信/手机号/课程/订单号">
								<span class="input-group-btn">
									<button class="btn btn-danger" type="submit">搜索</button>
      							</span>
							</form>
						</div>
						<div class="col-lg-6">
							<button type="button" class="btn btn-danger pull-right" onclick="location.href='<?php echo url('order/demand'); ?>'">备注要求</button> 
						</div>
					</div>
					<!-- /input-group -->
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>学员信息</th>
									<th>老师信息</th>
									<th>课程信息</th>
									<th>订单信息</th>
									<th width="100">订单管理</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<tr>
									<td class="center">
										账号：<?php echo $vo['saccount']; ?><br />
										昵称：<?php echo $vo['snickName']; ?><br />
										微信：<?php echo $vo['swx']; ?>
									</td>
									<td class="center">
										账号：<?php echo $vo['taccount']; ?><br />
										昵称：<?php echo $vo['tnickName']; ?><br />
										微信：<?php echo $vo['twx']; ?>
									</td>
									<td class="center">
										教材：<?php echo $vo['courseType']; ?><br />
										课程：<?php echo $vo['course']; ?> 第<?php echo $vo['class']; ?>课<br />
										课程名：<?php echo $vo['course_period']; ?><br />
										上课时间：<?php echo $vo['time']; ?>
									</td>
									<td class="center">
										订单编号：<?php echo $vo['id']; ?><br />
										下单时间：<?php echo $vo['create_time']; ?><br />
										备注要求：<?php echo $vo['demand']; ?><br />
										订单状态：<?php if($vo['state'] == 0): ?>
										待开始
										<?php elseif($vo['state'] == 1): ?>
										已完成(<a onclick="evaluate_info('<?php echo $vo['evaluate_info']; ?>')">授课评价<?php echo $vo['evaluate_score']; ?>分</a>)
										<?php elseif($vo['state'] == 2): ?>
										已取消(<?php echo $vo['reason_type']; ?>)
										<?php elseif($vo['state'] == 3): ?>
										待评价
										<?php endif; ?>
									</td>
									<td class="center">
										<?php if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
										<div class="btn-group-vertical" role="group" aria-label="...">
											<?php if($vo['thecancel'] == 1): ?>
											<button type="button" onclick="cancel(<?php echo $vo['id']; ?>)" class="btn btn-danger">取消订单</button>
											<?php else: ?>
											<button type="button" class="btn btn-default" onclick="alert('禁止取消10天前预约')" style="color: #c6c6c6;">取消订单</button>
											<?php endif; ?>
										</div>
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="box" id="box"></div>
						</div>
					</div>
				</div>
			</div>
			<!--End Advanced Tables -->
		</div>
	</div>
</div>
<div id="evaluate" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">评价内容</h4>
      </div>
      <div class="modal-body">
      	<p id="evaluate_info"></p>
      </div>
    </div>
  </div>
</div>

			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="__STATIC__/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="__STATIC__/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="__STATIC__/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="__STATIC__/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="__STATIC__/js/paging.js"></script>
		
<script>
	var setTotalCount = <?php echo $total; ?>;
	var p = <?php echo $p; ?>;
	window.onload = function() {
		if(GetQueryString('search')!=null){
			$('#search').val(GetQueryString('search'));
		}
	}
	$('#box').paging({
		initPageNo: <?php echo $p; ?>, // 初始页码
		totalPages: <?php echo $pageNum; ?>, //总页数
		totalCount: '共' + setTotalCount + '条数据', // 条目总数
		slideSpeed: 300, // 缓动速度。单位毫秒
		jump: false, //是否支持跳转
		callback: function(page) { // 回调函数
			if(page != p) {
				if(GetQueryString('search')!=null){
					window.location.replace("<?php echo url('order/index'); ?>" + '?p=' + page + '&state=<?php echo $state; ?>&search=' + GetQueryString('search'));
				}else{
					window.location.replace("<?php echo url('order/index'); ?>" + '?p=' + page + '&state=<?php echo $state; ?>');
				}
			}
		}
	})
</script>
<script>
	//取消订单
	function cancel(id) {
		info = '是否取消此订单?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('order/cancel'); ?>",
				data: {
					id: id
				},
				success: function(result) {
					if(result.data == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
	
	function evaluate_info(info){
		if(info == ''){
			info = '无评价内容';
		}
		$('#evaluate_info').html(info);
		$('#evaluate').modal('show');
	}
	
	function GetQueryString(key){
    		var url = window.location.search;
   		var reg = new RegExp("(^|&)"+ key +"=([^&]*)(&|$)");
    		var result = url.substr(1).match(reg);
    		return result ? decodeURIComponent(result[2]) : null;
	}
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>