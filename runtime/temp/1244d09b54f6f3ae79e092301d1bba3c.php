<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:114:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/course/add_video.html";i:1561364904;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1561295111;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="/static/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="/static/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="/static/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="/static/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="/static/css/paging.css" rel="stylesheet" />
		
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="recognition-menu" href="<?php echo url('/admin/recognition'); ?>"><i class="fa fa-list"></i> 语音识别题</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				

<div class="header">
	<h1 class="page-header">
		教材管理
		<small>添加视频</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li>
			<a href="<?php echo url('course/index'); ?>">教材管理</a>
		</li>
		<li>
			<a href="<?php echo url('/admin/course/period/id/'.$course_id); ?>">课时管理</a>
		</li>
		<li class="active">添加视频</li>
	</ol>
</div>
<div id="page-inner">
	<div class="row">
		<form id="video_form" class="col-md-12" method="post" action="<?php echo url('/admin/video', '', false); ?>">
            <input type="hidden" name="period_id" value="<?php echo $period_id; ?>">
			<input type="hidden" name="course_id" value="<?php echo $course_id; ?>">

			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="card-title">
						<div class="title">视频封面</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="form-horizontal" style="margin-bottom: 200px;">
						<label class="col-sm-2 control-label">请选择</label>
						<div class="col-sm-10">
							<div id="container_cover">
								<a class="btn btn-default btn-lg " id="pickfiles" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>图片</span>
								</a> jpg格式
							</div><br />
							<div>
								<div class="col-sm-4">
									<div id="img">
										<img class="img-responsive" width="200" src="/static/img/avatar.jpg " />
									</div>
									<input id="local_cover" type="hidden" value="/static/img/avatar.jpg" name="video_src[local][cover]" />
									<input id="youtube_cover" type="hidden" value="/static/img/avatar.jpg" name="video_src[youtube][cover]" />
									<div id="img-progress">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel-heading">
					<div class="card-title">
						<div class="title">本站视频上传</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="form-horizontal" style="margin-bottom: 200px;">
						<label class="col-sm-2 control-label">视频</label>
						<div class="col-sm-10">
							<div id="container-mp4">
								<a class="btn btn-default btn-lg " id="pickfiles-mp4" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>选择视频</span>
								</a> MP4格式
							</div>
							<div>
								<div class="col-sm-4">
									<div id="mp4"></div>
									<input id="divmp4" type="hidden" name="video_src[local][url]" />
									<div id="mp4-progress">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel-heading">
					<div class="card-title">
						<div class="title">Youtobe 视频</div>
					</div>
				</div>
				<div class="panel-body">
					<div class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-2 control-label">URL</label>
							<div class="col-sm-8">
								<input type="text" name="video_src[youtube][url]" class="form-control" required="required">
							</div>
						</div>
					</div>
				</div>

				<div class="panel-body">
					<button type="submit" class="btn btn-danger pull-right">添加视频</button>
				</div>
			</div>
		</form>
	</div>
</div>

			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="/static/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="/static/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="/static/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="/static/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="/static/js/paging.js"></script>
		
<script src="/static/Plupload/moxie.js"></script>
<script src="/static/Plupload/plupload.dev.js"></script>
<script src="/static/Plupload/i18n/zh_CN.js"></script>
<script src="/static/js/qiniu.min.js"></script>
<script>

	$("#video_form").on('submit', function (e) {
		$.post($(this).prop('action'), $(this).serialize(), function(data){
			if(data.code === 1) {
				location.href = data.url;
			}
		});

		return false;
	});

	var domain = "http://img.panpanchinese.cn/";
	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container_cover', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		mime_types: [{
			title: "Image files",
			extensions: "jpg"
		}],
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container_cover', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#img-progress').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("img").innerHTML =
					'<img id="img" class="img-responsive" width="200" src="' + domain + res.key + '" />';
				$('#cover, #youtube_cover, #local_cover').val(domain + res.key);
				$('#img-progress').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});

	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles-mp4', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container-mp4', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container-mp4', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#mp4-progress').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("mp4").innerHTML =
				'<video src="' + domain + res.key + '" width="320" controls="controls"></video>'; 
				$('#divmp4').val(domain + res.key);
				$('#mp4-progress').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/video') != -1) {
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recognition') != -1){
					$('#recognition-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>