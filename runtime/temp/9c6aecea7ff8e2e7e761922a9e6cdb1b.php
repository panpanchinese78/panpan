<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:118:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/authentication/index.html";i:1560143953;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1560719036;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="/static/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="/static/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="/static/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="/static/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="/static/css/paging.css" rel="stylesheet" />
		
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				

<div class="header">
	<h1 class="page-header">
		入驻申请
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li>
			<a href="<?php echo url('authentication/index'); ?>">入驻申请</a>
		</li>
	</ol>
 
</div>
<div id="page-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- Advanced Tables -->
			<div class="panel panel-default"><div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th width="100">用户头像</th>
									<th>账户信息</th>
									<th>用户信息</th>
									<th>自我介绍</th>
									<th>申请信息</th>
									<th>结算信息</th>
									<th width="100">身份证正面</th>
									<th width="100">身份证背面</th>
									<th width="100">申请管理</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<tr>
									<td><img src="<?php echo $vo['avatar']; ?>" class="img-responsive"></td>
									<td class="center">
										昵称：<?php echo $vo['nickName']; ?><br />
										账号：<?php echo $vo['account']; ?>
									</td>
									<td class="center">
										微信：<?php echo $vo['wx']; ?><br />
										手机：<?php echo $vo['phone']; ?><br />
										<?php if($vo['type'] == 0): ?>地区：<?php echo $vo['time_zone_name']; endif; ?>
									</td>
									<td class="center"><?php echo $vo['introduction']; ?></td>
									<td class="center">
										申请类型：<?php if($vo['type'] == 1): ?>老师<?php else: ?>学生<?php endif; ?><br />
										申请时间：<?php echo $vo['create_time']; ?>
									</td>
									<td class="center">
										<?php if($vo['type'] == 1): ?>
										真实姓名：<?php echo $vo['name']; ?><br />
										结算银行：<?php echo $vo['bank']; ?><br />
										结算账户：<?php echo $vo['bank_account']; else: ?>————<?php endif; ?>
									</td>
									<td>
										<?php if($vo['type'] == 1): ?>
										<img src="<?php echo $vo['identification1']; ?>" class="img-responsive">
										<?php else: ?>————<?php endif; ?>
									</td>
									<td>
										<?php if($vo['type'] == 1): ?>
										<img src="<?php echo $vo['identification2']; ?>" class="img-responsive">
										<?php else: ?>————<?php endif; ?>
									</td>
									<td class="center">
										<div class="btn-group-vertical" role="group" aria-label="...">
											<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
											<button type="button" onclick="pass(<?php echo $vo['id']; ?>)" class="btn btn-danger">审核通过</button>
											<button type="button" onclick="refuse(<?php echo $vo['id']; ?>)" class="btn btn-danger">拒绝通过</button>
											<?php endif; ?>
										</div>
									</td>
								</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="box" id="box"></div>
						</div>
					</div>
				</div>
			</div>
			<!--End Advanced Tables -->
		</div>
	</div>
</div>

			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="/static/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="/static/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="/static/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="/static/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="/static/js/paging.js"></script>
		
<script>
	var setTotalCount = <?php echo $total; ?>;
	var p = <?php echo $p; ?>;
	$('#box').paging({
		initPageNo: <?php echo $p; ?>, // 初始页码
		totalPages: <?php echo $pageNum; ?>, //总页数
		totalCount: '共' + setTotalCount + '条数据', // 条目总数
		slideSpeed: 300, // 缓动速度。单位毫秒
		jump: false, //是否支持跳转
		callback: function(page) { // 回调函数
			if(page != p) {
				window.location.replace("<?php echo url('authentication/index'); ?>" + '?p=' + page + '&type=<?php echo $type; ?>');
			}
		}
	})
</script>
<script>
	//审核通过
	function pass(id) {
		info = '是否审核通过?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('authentication/pass'); ?>",
				data: {
					id: id
				},
				success: function(result) {
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
</script>
<script>
	//审核拒绝
	function refuse(id) {
		info = '是否审核拒绝?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('authentication/refuse'); ?>",
				data: {
					id: id
				},
				success: function(result) {
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/video') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>