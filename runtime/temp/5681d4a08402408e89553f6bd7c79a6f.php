<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:110:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/course/index.html";i:1560143954;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1561295111;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="/static/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="/static/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="/static/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="/static/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="/static/css/paging.css" rel="stylesheet" />
		
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="recognition-menu" href="<?php echo url('/admin/recognition'); ?>"><i class="fa fa-list"></i> 语音识别题</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				

<div class="header">
	<h1 class="page-header">
		教材管理
	<?php switch(\think\Request::instance()->param('state')): case "1": ?><small>可预约</small><?php break; case "0": ?><small>已关闭</small><?php break; default: ?>
    		<small>全部教材</small>
	<?php endswitch; ?>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li>
			<a href="<?php echo url('course/index'); ?>">教材管理</a>
		</li>
		<?php switch(\think\Request::instance()->param('state')): case "1": ?>
		<li class="active">可预约</li><?php break; case "0": ?>
		<li class="active">已关闭</li><?php break; default: ?>
		<li class="active">全部教材</li>
		<?php endswitch; ?>
	</ol>
 
</div>
<div id="page-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs">
			<?php switch(\think\Request::instance()->param('state')): case "1": ?>
				<li><a href="<?php echo url('course/index'); ?>">全部教材</a></li>
				<li class="active"><a href="<?php echo url('course/index',['state'=>1]); ?>">可预约</a></li>
				<li><a href="<?php echo url('course/index',['state'=>0]); ?>">已关闭</a></li>
				<?php break; case "0": ?>
				<li><a href="<?php echo url('course/index'); ?>">全部教材</a></li>
				<li><a href="<?php echo url('course/index',['state'=>1]); ?>">可预约</a></li>
				<li class="active"><a href="<?php echo url('course/index',['state'=>0]); ?>">已关闭</a></li>
				<?php break; default: ?>
				<li class="active"><a href="<?php echo url('course/index'); ?>">全部教材</a></li>
				<li><a href="<?php echo url('course/index',['state'=>1]); ?>">可预约</a></li>
				<li><a href="<?php echo url('course/index',['state'=>0]); ?>">已关闭</a></li>
			<?php endswitch; ?>
			</ul>
			<!-- Advanced Tables -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-12"> 
							<button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#addCourse">新增教材</button>
							<button type="button" class="btn btn-danger pull-right" style="margin-right: 10px;" data-toggle="modal" data-target="#typeList">分类管理</button>
							<button type="button" class="btn btn-danger pull-right" style="margin-right: 10px;" data-toggle="modal" data-target="#addType">新增分类</button>
						</div>
					</div>
					<!-- /input-group -->
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th width="120">教材封面</th>
									<th width="120">英文封面</th>
									<th>教材分类</th>
									<th>教材名称（中文）</th>
									<th>教材名称（英文）</th>
									<th>教材名称（韩文）</th>
									<th>课时数量</th>
									<th>教材状态</th>
									<th width="100">教材管理</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<tr>
									<td class="center"><img class="img-responsive" width="100" height="150" src="<?php echo $vo['cover']; ?>" /></td> 
									<td class="center"><img class="img-responsive" width="100" height="150" src="<?php echo $vo['cover_en']; ?>" /></td> 
									<td><?php echo $vo['type_name']; ?></td>
									<td class="center"><?php echo $vo['name_cn']; ?></td>
									<td class="center"><?php echo $vo['name_en']; ?></td>
									<td class="center"><?php echo $vo['name_ko']; ?></td>
									<td class="center"><?php echo $vo['period']; ?></td>
									<td class="center"><?php if($vo['state'] == 0): ?>已关闭<?php else: ?>可预约<?php endif; ?></td>
									<td class="center">
										<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
										<div class="btn-group-vertical" role="group" aria-label="...">
											<button type="button" onclick="editCourse('<?php echo $vo['id']; ?>','<?php echo $vo['type']; ?>','<?php echo $vo['state']; ?>','<?php echo $vo['cover']; ?>','<?php echo $vo['cover_en']; ?>')" class="btn btn-danger">教材管理</button>
											<button type="button" onclick="location.href='<?php echo url('course/period',['id'=>$vo['id']]); ?>'" class="btn btn-danger">课程管理</button>
											<button type="button" onclick="deletecourse('<?php echo $vo['id']; ?>')" class="btn btn-danger">删除</button>
										</div>
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="box" id="box"></div>
						</div>
					</div>
				</div>
			</div>
			<!--End Advanced Tables -->
		</div>
	</div>
</div>
<div id="typeList" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">分类管理</h4>
      </div>
      <div class="modal-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>中文名称</th>
									<th>英文名称</th>
									<th>韩文名称</th>
									<th width="100">分类管理</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($CourseType) || $CourseType instanceof \think\Collection || $CourseType instanceof \think\Paginator): $i = 0; $__LIST__ = $CourseType;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<tr> 
									<td class="center"><?php echo $vo['name']; ?></td>
									<td class="center"><?php echo $vo['name_en']; ?></td>
									<td class="center"><?php echo $vo['name_ko']; ?></td>
									<td class="center">
										<div class="btn-group-vertical" role="group" aria-label="...">
											<button type="button" onclick="deletetype('<?php echo $vo['id']; ?>')" class="btn btn-danger">删除</button>
										</div>
									</td>
								</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
     </div>
    </div>
  </div>
</div>
<div id="addType" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">新增分类</h4>
      </div>
      <div class="modal-body">
		<div class="form-group">
        <input type="text" class="form-control" id="addTypeCN" placeholder="中文名称">
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="addTypeEN" placeholder="英文名称">
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="addTypeKO" placeholder="韩文名称">
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" onclick="addType()">保存</button>
      </div>
    </div>
  </div>
</div>
<div id="addCourse" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">新增教材</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 300px;">
						<div class="col-sm-12">
							<div id="container">
								<a class="btn btn-default btn-lg " id="pickfiles" style="width: 100%;" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>选择封面图(jpg格式)</span>
								</a>
							</div>
							<br />
							<small>200X300像素</small>
							<div>
								<div class="col-sm-12">
									<div id="img">
										<img class="img-responsive" width="200" height="300" src="/static/img/avatar.jpg " />
									</div>
									<input id="addCourseCover" type="hidden" />
									<div id="img-progress">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<div id="container2">
								<a class="btn btn-default btn-lg " id="pickfiles2" style="width: 100%;" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>英文封面图(jpg格式)</span>
								</a>
							</div>
							<br />
							<small>200X300像素</small>
							<div>
								<div class="col-sm-12">
									<div id="img2">
										<img class="img-responsive" width="200" height="300" src="/static/img/avatar.jpg " />
									</div>
									<input id="addCourseCover2" type="hidden" />
									<div id="img-progress2">
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
			<div class="col-md-6">
		<div class="form-group">
      	<select id="addCourseType" class="form-control">
      		<?php if(is_array($CourseType) || $CourseType instanceof \think\Collection || $CourseType instanceof \think\Paginator): $i = 0; $__LIST__ = $CourseType;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ct): $mod = ($i % 2 );++$i;?>
			<option value="<?php echo $ct['id']; ?>"><?php echo $ct['name']; ?></option>
			<?php endforeach; endif; else: echo "" ;endif; ?>
		</select>
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="addCourseCn" placeholder="教材名称（中文）">
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="addCourseKo" placeholder="教材名称（韩文）">
		</div>
		<div class="form-group">
        <input type="text" class="form-control" id="addCourseEn" placeholder="教材名称（英文）">
		</div>
		</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" onclick="addCourse()">保存</button> 
      </div>
    </div>
  </div>
</div>
<div id="editCourse" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title">教材管理</h4>
      </div>
      <div class="modal-body">
      	<input type="hidden" id="editCourseId" />
      	<div class="row">
      		<div class="col-md-6">
					<div class="form-group" style="margin-bottom: 300px;">
						<div class="col-sm-12">
							<div id="container-edit">
								<a class="btn btn-default btn-lg " id="pickfiles-edit" style="width: 100%;" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>选择封面图(jpg格式)</span>
								</a>
							</div>
							<br />
							<small>200X300像素</small>
							<div>
								<div class="col-sm-12">
									<div id="img-edit">
										<img class="img-responsive" width="200" height="300" src="/static/img/avatar.jpg " />
									</div>
									<input id="editCourseCover" type="hidden" />
									<div id="img-edit-progress">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<div id="container-edit2">
								<a class="btn btn-default btn-lg " id="pickfiles-edit2" style="width: 100%;" href="#">
									<i class="glyphicon glyphicon-plus"></i>
									<span>英文封面图(jpg格式)</span>
								</a>
							</div>
							<br />
							<small>200X300像素</small>
							<div>
								<div class="col-sm-12">
									<div id="img-edit2">
										<img class="img-responsive" width="200" height="300" src="/static/img/avatar.jpg " />
									</div>
									<input id="editCourseCover2" type="hidden" />
									<div id="img-edit-progress2">
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
			<div class="col-md-6">
		<div class="form-group">
      	<select id="editCourseType" class="form-control">
      		<?php if(is_array($CourseType) || $CourseType instanceof \think\Collection || $CourseType instanceof \think\Paginator): $i = 0; $__LIST__ = $CourseType;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$ct): $mod = ($i % 2 );++$i;?>
			<option value="<?php echo $ct['id']; ?>"><?php echo $ct['name']; ?></option>
			<?php endforeach; endif; else: echo "" ;endif; ?>
		</select>
		</div>
		<div class="form-group">
      	<select id="editCourseState" class="form-control">
			<option value="0">已关闭</option>
			<option value="1">可预约</option>
		</select>
		</div>
		</div>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button type="button" class="btn btn-primary" onclick="update()">保存</button> 
      </div>
    </div>
  </div>
</div>

			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="/static/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="/static/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="/static/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="/static/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="/static/js/paging.js"></script>
		
<script src="/static/Plupload/moxie.js"></script>
<script src="/static/Plupload/plupload.dev.js"></script>
<script src="/static/Plupload/i18n/zh_CN.js"></script>
<script src="/static/js/qiniu.min.js"></script>
<script>
	var setTotalCount = <?php echo $total; ?>;
	var p = <?php echo $p; ?>;
	$('#box').paging({
		initPageNo: <?php echo $p; ?>, // 初始页码
		totalPages: <?php echo $pageNum; ?>, //总页数
		totalCount: '共' + setTotalCount + '条数据', // 条目总数
		slideSpeed: 300, // 缓动速度。单位毫秒
		jump: false, //是否支持跳转
		callback: function(page) { // 回调函数
			if(page != p) {
				window.location.replace("<?php echo url('course/index'); ?>" + '?p=' + page + '&state=<?php echo $state; ?>');
			}
		}
	})
</script>
<script>
	//新增分类
	function addType() {
		var name = $('#addTypeCN').val();
		var name_en = $('#addTypeEN').val();
		var name_ko = $('#addTypeKO').val();
		if(name.length < 1||name_en.length < 1||name_ko.length < 1){
			alert('请正确输入分类名');
			return false;
		}
		info = '是否新增?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('course/add_type'); ?>",
				data: {
					name: name,
					name_en: name_en,
					name_ko: name_ko
				},
				success: function(result) {
					$('#addType').modal('hide');
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
</script>
<script>
	//新增教材
	function addCourse() {
		var type = $('#addCourseType').val();
		var cn = $('#addCourseCn').val();
		var ko = $('#addCourseKo').val();
		var en = $('#addCourseEn').val();
		var cover = $('#addCourseCover').val();
		var cover_en = $('#addCourseCover2').val();
		if(cn.length < 1||ko.length < 1||en.length < 1){
			alert('请正确输入教材名称');
			return false;
		}
		info = '是否新增?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('course/add'); ?>",
				data: {
					type: type,
					name_cn: cn,
					name_ko: ko,
					name_en: en,
					cover: cover,
					cover_en: cover_en,
				},
				success: function(result) {
					$('#addCourse').modal('hide');
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
</script>
<script>
	//更新
	function editCourse(id,type,state,cover,cover_en) {
		$('#editCourseId').val(id);
		$("#editCourseType").val(type);
		$("#editCourseState").val(state);
		$('#img-edit').html('<img class="img-responsive" width="200" height="300" src="' + cover + '" />');
		$("#editCourseCover").val(cover);
		$('#img-edit2').html('<img class="img-responsive" width="200" height="300" src="' + cover_en + '" />');
		$("#editCourseCover2").val(cover_en);
		$("#editCourseState").val(state);
		$('#editCourse').modal('show');
	}
</script>
<script>
	//确认更新
	function update() {
		var id = $('#editCourseId').val();
		var type = $('#editCourseType').val();
		var cover = $('#editCourseCover').val();
		var cover_en = $('#editCourseCover2').val();
		var state = $('#editCourseState').val();
		info = '是否修改?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('course/edit'); ?>",
				data: {
					id: id,
					type: type,
					cover: cover,
					cover_en: cover_en,
					state: state
				},
				success: function(result) {
					$('#editCourse').modal('hide');
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
	//删除教材
	function deletecourse(id){
		info = '是否确认删除?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('course/delcourse'); ?>",
				data: {
					id: id
				},
				success: function(result) {
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
	//删除分类
	function deletetype(id){
		info = '是否确认删除?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('course/deltype'); ?>",
				data: {
					id: id
				},
				success: function(result) {
					if(result == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
	
	var domain = "http://img.panpanchinese.cn/";
	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		mime_types: [{
			title: "Image files",
			extensions: "jpg"
		}],
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#img-progress').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("img").innerHTML =
					'<img class="img-responsive" width="200" height="300" src="' + domain + res.key + '?imageView2/1/w/200/h/200/interlace/1" />';
				$('#addCourseCover').val(domain + res.key + '?imageView2/1/w/200/h/300/interlace/1');
				$('#img-progress').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});
	
	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles2', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container2', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		mime_types: [{
			title: "Image files",
			extensions: "jpg"
		}],
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container2', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#img-progress2').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("img2").innerHTML =
					'<img class="img-responsive" width="200" height="300" src="' + domain + res.key + '?imageView2/1/w/200/h/200/interlace/1" />';
				$('#addCourseCover2').val(domain + res.key + '?imageView2/1/w/200/h/300/interlace/1');
				$('#img-progress2').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});
	
	
	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles-edit', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container-edit', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		mime_types: [{
			title: "Image files",
			extensions: "jpg"
		}],
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container-edit', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#img-edit-progress').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("img-edit").innerHTML =
					'<img class="img-edit-responsive" width="200" height="300" src="' + domain + res.key + '?imageView2/1/w/200/h/200/interlace/1" />';
				$('#editCourseCover').val(domain + res.key + '?imageView2/1/w/200/h/300/interlace/1');
				$('#img-edit-progress').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});
	
	//引入Plupload 、qiniu.js后
	var uploader = Qiniu.uploader({
		runtimes: 'html5,flash,html4', //上传模式,依次退化
		browse_button: 'pickfiles-edit2', //上传选择的点选按钮，**必需**
		uptoken_url: "<?php echo url('image/gettoken'); ?>", //Ajax请求upToken的Url，**强烈建议设置**（服务端提供）
		// uptoken : '', //若未指定uptoken_url,则必须指定 uptoken ,uptoken由其他程序生成
		unique_names: true, // 默认 false，key为文件名。若开启该选项，SDK为自动生成上传成功后的key（文件名）。
		// save_key: true,   // 默认 false。若在服务端生成uptoken的上传策略中指定了 `sava_key`，则开启，SDK会忽略对key的处理
		domain: 'http://img.panpanchinese.cn/', //bucket 域名，下载资源时用到，**必需**
		get_new_uptoken: true, //设置上传文件的时候是否每次都重新获取新的token
		container: 'container-edit2', //上传区域DOM ID，默认是browser_button的父元素，
		max_file_size: '100mb', //最大文件体积限制
		multi_selection: false,
		mime_types: [{
			title: "Image files",
			extensions: "jpg"
		}],
		flash_swf_url: '/static/Plupload/Moxie.swf', //引入flash,相对路径
		max_retries: 3, //上传失败最大重试次数
		dragdrop: true, //开启可拖曳上传
		drop_element: 'container-edit2', //拖曳上传区域元素的ID，拖曳文件或文件夹后可触发上传
		chunk_size: '4mb', //分块上传时，每片的体积
		auto_start: true, //选择文件后自动上传，若关闭需要自己绑定事件触发上传
		init: {
			'FilesAdded': function(up, files) {
				plupload.each(files, function(file) {
					// 文件添加进队列后,处理相关的事情
				});
			},
			'BeforeUpload': function(up, file) {
				// 每个文件上传前,处理相关的事情
			},
			'UploadProgress': function(up, file) {
				var total = up.total;
				$('#img-edit-progress2').html('<div class="progress"><div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="' + total.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + total.percent + '%"></div></div>');
				// 每个文件上传时,处理相关的事情
			},
			'FileUploaded': function(up, file, info) {
				// 每个文件上传成功后,处理相关的事情
				// 其中 info.response 是文件上传成功后，服务端返回的json，形式如
				// {
				//    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
				//    "key": "gogopher.jpg"
				//  }
				// 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
				var domain = up.getOption('domain');
				var res = JSON.parse(info.response);
				document.getElementById("img-edit2").innerHTML =
					'<img class="img-edit-responsive" width="200" height="300" src="' + domain + res.key + '?imageView2/1/w/200/h/200/interlace/1" />';
				$('#editCourseCover2').val(domain + res.key + '?imageView2/1/w/200/h/300/interlace/1');
				$('#img-edit-progress2').html('');
			},
			'Error': function(up, err, errTip) {
				//上传出错时,处理相关的事情
			},
			'UploadComplete': function() {
				//队列文件处理完毕后,处理相关的事情
			},
			'Key': function(up, file) {
				// 若想在前端对每个文件的key进行个性化处理，可以配置该函数
				// 该配置必须要在 unique_names: false , save_key: false 时才生效

				var key = "";
				// do something with key here
				return key
			}
		}
	});
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/video') != -1) {
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recognition') != -1){
					$('#recognition-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>