<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:114:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/curriculum/index.html";i:1560143950;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1560143951;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="__STATIC__/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="__STATIC__/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="__STATIC__/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="__STATIC__/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="__STATIC__/css/paging.css" rel="stylesheet" />
		
<link href="__STATIC__/css/bootstrap-datetimepicker.min.css" rel="stylesheet" /> 
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				

<div class="header">
	<h1 class="page-header">
		课程管理
	<?php switch(\think\Request::instance()->param('state')): case "1": ?><small>已预约</small><?php break; case "0": ?><small>未预约</small><?php break; default: ?>
    		<small>全部</small>
	<?php endswitch; ?>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li class="active">课程管理</li>
	</ol>
 
</div>
<div id="page-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- Advanced Tables -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<div class="col-lg-6">
						</div>
						<div class="col-lg-6"> 
							<button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#screen">筛选</button>
						</div>
					</div>
					<!-- /input-group -->
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>上课时间</th>
									<th>课程状态</th>
									<th>老师信息</th>
									<th>课程信息</th>
									<th>学生信息</th>
									<th>订单信息</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<tr>
									<td>
										<?php echo $vo['interval']; ?>
									</td>
									<td class="center">
										<?php echo $vo['stateinfo']; ?>
									</td>
									<td class="center">
										账号:<?php echo $vo['teacher_account']; ?><br />
										昵称:<?php echo $vo['teacher_nickName']; ?><br />
										微信:<?php echo $vo['teacher_wx']; ?>
									</td>
									<td class="center">
										<?php if($vo['state'] == 0): ?>
										————
										<?php else: ?>
										教材:<?php echo $vo['courseType']; ?><br />
										课程:<?php echo $vo['course']; ?>&nbsp;&nbsp;第<?php echo $vo['class']; ?>课
										<?php endif; ?>
									</td>
									<td class="center">
										<?php if($vo['student_id'] == 0): ?>
										————
										<?php else: ?>
										账号:<?php echo $vo['student_account']; ?><br />
										昵称:<?php echo $vo['student_nickName']; ?><br />
										微信:<?php echo $vo['student_wx']; endif; ?>
									</td>
									<td class="center">
										<?php if($vo['state'] == 0): ?>
										————
										<?php else: ?>
										订单编号:<?php echo $vo['order_id']; ?><br />
										下单时间:<?php echo $vo['order_create_time']; ?><br />
										订单状态:<?php echo $vo['order_stateinfo']; endif; ?>
									</td>
								</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="box" id="box"></div>
						</div>
					</div>
				</div>
			</div>
			<!--End Advanced Tables -->
		</div>
	</div>
</div>
<div class="modal fade" id="screen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <form class="modal-content" method="get">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">筛选</h4>
      </div>
      <div class="modal-body">
      <div class="form-horizontal">
		<div class="form-group">
			<label class="col-sm-2 control-label">起始日期</label>
			<div class="col-sm-10">
				<input type="text" name="stime" class="form-control" id="stime">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">结束日期</label>
			<div class="col-sm-10">
				<input type="text" name="etime" class="form-control" id="etime">
			</div>
  		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">星期</label>
			<div class="col-sm-10">
				<label class="checkbox-inline">
					<input type="checkbox" name="week[]" value="1"> 周一
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="week[]" value="2"> 周二
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="week[]" value="3"> 周三
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="week[]" value="4"> 周四
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="week[]" value="5"> 周五
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="week[]" value="6"> 周六
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="week[]" value="0"> 周日
				</label> 
			</div>
  		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">时段</label>
			<div class="col-sm-10">
				<label class="checkbox-inline">
					<input type="checkbox" name="time[]" value="1,2,3,4,5,6,7,8,9,10,11,12"> 00:30-06:00
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="time[]" value="13,14,15,16,17,18,19,20,21,22,23,24"> 06:30-12:00
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="time[]" value="25,26,27,28,29,30,31,32,33,34,35,36"> 12:30-18:00
				</label>
				<label class="checkbox-inline">
					<input type="checkbox" name="time[]" value="37,38,39,40,41,42,43,44,45,46,47,0"> 18:30-0:00
				</label>
			</div>
  		</div>
	  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <button type="submit" class="btn btn-primary">确定</button>
      </div>
    </form>
  </div>
</div>

			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="__STATIC__/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="__STATIC__/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="__STATIC__/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="__STATIC__/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="__STATIC__/js/paging.js"></script>
		
<script src="__STATIC__/js/bootstrap-datetimepicker.min.js"></script>
<script src="__STATIC__/js/bootstrap-datetimepicker.zh-CN.js"></script>
<script>
	$('#stime').datetimepicker({
		minView: "month",
		language: 'zh-CN',
		format: 'yyyy-mm-dd 00:00:00'
	});
	$('#etime').datetimepicker({
		minView: "month",
		language: 'zh-CN',
		format: 'yyyy-mm-dd 00:00:00'
	});
	var setTotalCount = <?php echo $total; ?>;
	var p = <?php echo $p; ?>;
	window.onload = function() {
		if(GetQueryString('search')!=null){
			$('#search').val(GetQueryString('search'));
		}
	}
	$('#box').paging({
		initPageNo: <?php echo $p; ?>, // 初始页码
		totalPages: <?php echo $pageNum; ?>, //总页数
		totalCount: '共' + setTotalCount + '条数据', // 条目总数
		slideSpeed: 300, // 缓动速度。单位毫秒
		jump: false, //是否支持跳转
		callback: function(page) { // 回调函数
			if(page != p) {
				if(GetQueryString('stime')!=null||GetQueryString('etime')!=null||GetQueryString('week')!=null||GetQueryString('time')!=null){
					var theinfo = window.location.search;
					window.location.replace("<?php echo url('curriculum/index'); ?>" + theinfo + '&p=' + page);
				}else{
					window.location.replace("<?php echo url('curriculum/index'); ?>" + '?p=' + page + '&state=<?php echo $state; ?>');
				}
			}
		}
	})
	
	function GetQueryString(key){
    		var url = window.location.search;
   		var reg = new RegExp("(^|&)"+ key +"=([^&]*)(&|$)");
    		var result = url.substr(1).match(reg);
    		return result ? decodeURIComponent(result[2]) : null;
	}
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>