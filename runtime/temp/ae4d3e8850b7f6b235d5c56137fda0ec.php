<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:112:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/web/../application/admin/view/recharge/index.html";i:1560143954;s:102:"/Users/sudongyan/devspace/panpanchinese.cn/m2_panpanchinese.cn/application/admin/view/Base/common.html";i:1560143951;}*/ ?>
<!DOCTYPE html>
<html lang="zh-cn">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta content="" name="description" />
		<meta content="webthemez" name="author" />
		<title>一对一课程管理系统</title>
		<!-- Bootstrap Styles-->
		<link href="__STATIC__/css/bootstrap.css" rel="stylesheet" />
		<!-- FontAwesome Styles-->
		<link href="__STATIC__/css/font-awesome.css" rel="stylesheet" />
		<!-- Morris Chart Styles-->
		<link href="__STATIC__/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
		<!-- Custom Styles-->
		<link href="__STATIC__/css/custom-styles.css" rel="stylesheet" />
		<!-- page-->
		<link href="__STATIC__/css/paging.css" rel="stylesheet" />
		
	</head>

	<body>
		<div id="wrapper">
			<nav class="navbar navbar-default top-navbar" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
					<a class="navbar-brand" href="<?php echo url('index/index'); ?>"><strong>一对一课程</strong></a>

					<div id="sideNav" href="">
						<i class="fa fa-bars icon"></i>
					</div>
				</div>

				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							<i class="fa fa-user fa-fw"></i> <i class="fa"><?php echo \think\Session::get('account'); ?></i> <i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li>
								<a href="<?php echo url('admin/password'); ?>"><i class="fa fa-gear fa-fw"></i> 修改密码</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="<?php echo url('login/out'); ?>"><i class="fa fa-sign-out fa-fw"></i> 退出登录</a>
							</li>
						</ul>
						<!-- /.dropdown-user -->
					</li>
					<!-- /.dropdown -->
				</ul>
			</nav>
			<!--/. NAV TOP  -->
			<nav class="navbar-default navbar-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="main-menu">
						<?php if((\think\Session::get('authentication') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="authentication-menu" href="<?php echo url('authentication/index',['state'=>0]); ?>"><i class="fa fa-list"></i> 入驻申请</a>
						</li>
						<?php endif; if((\think\Session::get('order') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="order-menu" href="<?php echo url('order/index'); ?>"><i class="fa fa-list"></i> 预约管理</a>
						</li>
						<?php endif; if((\think\Session::get('teacher') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="teacher-menu" href="<?php echo url('teacher/index'); ?>"><i class="fa fa-list"></i> 老师管理</a>
						</li>
						<?php endif; if((\think\Session::get('student') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="student-menu" href="<?php echo url('student/index'); ?>"><i class="fa fa-list"></i> 学员管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="curriculum-menu" href="<?php echo url('curriculum/index'); ?>"><i class="fa fa-list"></i> 课程管理</a>
						</li>
						<?php if((\think\Session::get('course') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="course-menu" href="<?php echo url('course/index'); ?>"><i class="fa fa-list"></i> 教材管理</a>
						</li>
						<?php endif; if((\think\Session::get('timezone') == 1) OR (\think\Session::get('id') == 1)): ?>
						<li>
							<a id="timezone-menu" href="<?php echo url('timezone/index'); ?>"><i class="fa fa-list"></i> 时区管理</a>
						</li>
						<?php endif; ?>
						<li>
							<a id="settlement-menu" href="<?php echo url('settlement/index'); ?>"><i class="fa fa-list"></i> 结算管理</a>
						</li>
						<li>
							<a id="recharge-menu" href="<?php echo url('recharge/index'); ?>"><i class="fa fa-list"></i> 充值管理</a>
						</li>
						<?php if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="price-menu" href="<?php echo url('price/index'); ?>"><i class="fa fa-list"></i> 套餐管理</a>
						</li>
						<?php endif; if(\think\Session::get('id') == 1): ?>
						<li>
							<a id="admin-menu" href="<?php echo url('admin/index'); ?>"><i class="fa fa-list"></i> 管理员</a>
						</li>
						<?php endif; ?>
					</ul>

				</div>

			</nav>
			<div id="page-wrapper">
				

<div class="header">
	<h1 class="page-header">
		充值管理
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="<?php echo url('index/index'); ?>">首页</a>
		</li>
		<li>
			<a href="<?php echo url('recharge/index'); ?>">充值管理</a>
		</li>
	</ol>

</div>
<div id="page-inner">
	<div class="row">
		<div class="col-md-12">
			<!-- Advanced Tables -->
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>学员信息</th>
									<th>订单信息</th>
									<th>费用信息</th>
									<th>操作人员</th>
									<th width="100">充值管理</th>
								</tr>
							</thead>
							<tbody>
								<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
								<tr>
									<td class="center">
										账号：<?php echo $vo['student_account']; ?><br />
										昵称：<?php echo $vo['student_nickName']; ?><br />
										微信：<?php echo $vo['student_wx']; ?><br />
										电话：<?php echo $vo['student_phone']; ?>
									</td>
									<td>
										套餐名称：<?php echo $vo['name']; ?><br />
										套餐时效：<?php echo $vo['day']; ?>天<br />
										套餐内容：<?php echo $vo['class']; ?>课时/月<br />
										下单时间：<?php echo $vo['create_time']; ?>
									</td>
									<td class="center">
										套餐费用：<?php if($vo['type'] == 0): if($vo['usd_discount_money'] != null): ?><?php echo $vo['usd_discount_money']; ?>美元<?php else: ?><?php echo $vo['discount_money']; ?>韩元<?php endif; elseif($vo['type'] == 3): ?><?php echo $vo['discount_money']; ?>韩元<?php elseif($vo['type'] == 1): ?><?php echo $vo['discount_money']; ?>韩元<?php elseif($vo['type'] == 2): if($vo['cn_discount_money'] != null): ?><?php echo $vo['cn_discount_money'] / 100; ?>人民币<?php else: ?><?php echo $vo['discount_money']; ?>韩元<?php endif; endif; ?><br />
										支付方式：<?php if($vo['type'] == 0): ?>PAYPAL<?php elseif($vo['type'] == 3): ?>韩币支付<?php elseif($vo['type'] == 1): ?>转账<?php elseif($vo['type'] == 2): ?>微信支付<?php endif; ?><br />
										<?php if($vo['type'] == 1 OR $vo['type'] == 3): ?>汇款者姓名：<?php echo $vo['payer_name']; ?><br /><?php endif; ?>
										优惠券ID：<?php if($vo['coupon_id'] == 0): ?>未使用<?php else: ?><?php echo $vo['coupon_id']; endif; ?><br />
										开始时间：<?php echo $vo['payment_time']; ?><br />
										结束时间：<?php echo $vo['end_time']; ?>
									</td>
									<td class="center">
										<?php if($vo['type'] == 0): ?>
										账号：<?php echo $vo['student_account']; ?><br />
										昵称：<?php echo $vo['student_nickName']; ?><br />
										<?php else: ?>
										管理员账号：<?php echo $vo['admin_account']; ?><br />
										管理员昵称：<?php echo $vo['admin_nickName']; ?><br />
										<?php endif; ?>
										操作时间：<?php if($vo['type'] == 0): ?><?php echo $vo['create_time']; else: ?><?php echo $vo['payment_time']; endif; ?><br />
										操作结果：<?php if($vo['state'] == 0): ?>充值失败<?php else: ?>充值成功<?php endif; ?>
									</td>
									<td class="center">
										<?php if(($vo['type'] == 1) AND ($vo['state'] == 0)): ?>
										<button type="button" onclick="theconfirm(<?php echo $vo['order_id']; ?>);" class="btn btn-danger">确认收款</button>
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="box" id="box"></div>
						</div>
					</div>
				</div>
			</div>
			<!--End Advanced Tables -->
		</div>
	</div>
</div>

			</div>
		</div>
		<!-- /. WRAPPER  -->
		<!-- JS Scripts-->
		<!-- jQuery Js -->
		<script src="__STATIC__/js/jquery-1.10.2.js"></script>
		<!-- Bootstrap Js -->
		<script src="__STATIC__/js/bootstrap.min.js"></script>
		<!-- Metis Menu Js -->
		<script src="__STATIC__/js/jquery.metisMenu.js"></script>
		<!-- Custom Js -->
		<script src="__STATIC__/js/custom-scripts.js"></script>
		<!-- page -->
		<script src="__STATIC__/js/paging.js"></script>
		
<script>
	var setTotalCount = <?php echo $total; ?>;
	var p = <?php echo $p; ?>;
	$('#box').paging({
		initPageNo: <?php echo $p; ?>, // 初始页码
		totalPages: <?php echo $pageNum; ?>, //总页数
		totalCount: '共' + setTotalCount + '条数据', // 条目总数
		slideSpeed: 300, // 缓动速度。单位毫秒
		jump: false, //是否支持跳转
		callback: function(page) { // 回调函数
			if(page != p){
				window.location.replace("<?php echo url('recharge/index'); ?>" + '?p=' + page);
			}
		}
	})
	
	function theconfirm(order_id){
		info = '是否确认收款?';
		if(confirm(info)) {
			$.ajax({
				type: "POST",
				url: "<?php echo url('recharge/confirm'); ?>",
				data: {
					order_id: order_id
				},
				success: function(result) {
					if(result.data == 1) {
						location.reload();
					} else {
						alert('发生错误');
					}
				}
			});
		}
	}
</script>

		<!--去掉按钮或链接点击边框-->
		<script type="text/javascript">
			$("a,button,input:submit,input:button").focus(function() {
				this.blur()
			});
		</script>
		<script>
			$(function() {
				//导航选中
				var url = window.location.href;
				if(url.indexOf('/admin/admin') != -1) {
					$('#admin-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/price/') != -1){
					$('#price-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teacher/') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/teachertag') != -1){
					$('#teacher-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/student') != -1){
					$('#student-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/timezone') != -1){
					$('#timezone-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/course') != -1){
					$('#course-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/curriculum') != -1){
					$('#curriculum-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/order') != -1){
					$('#order-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/settlement') != -1){
					$('#settlement-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/recharge') != -1){
					$('#recharge-menu').addClass('active-menu');
				}else if(url.indexOf('/admin/authentication') != -1){
					$('#authentication-menu').addClass('active-menu');
				}else{
					$('#index-menu').addClass('active-menu');
				}
			});
		</script>
	</body>

</html>