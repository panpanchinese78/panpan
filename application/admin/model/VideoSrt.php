<?php

namespace app\admin\model;

use think\Model;

class VideoSrt extends Model
{

    protected $type = [
        'create_time' => 'datetime',
        'update_time' => 'datetime',
        'is_asr' => 'boolean',
        'is_fill' => 'boolean',
    ];

    protected $autoWriteTimestamp = 'datetime';

}
