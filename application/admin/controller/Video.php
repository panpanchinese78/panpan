<?php

namespace app\admin\controller;

use think\Controller;
use app\admin\model\Video as VideoModel;
use Gaoming13\HttpCurl\HttpCurl;
use think\Request;

class Video extends Controller
{
    //初始执行
    protected function _initialize()
    {
        action('Common/all');
        if (session('video') == 0 && session('id') != 1) {
            $this->error('无权限');
        }
    }

    //视频列表
    public function index(Request $request)
    {
        $input = input('get.');
        $videoModel = VideoModel::where('period_id', $input['period'])
            ->where('course_id', $input['course'])
            ->find();
        return view('course/video', $videoModel->toArray());

    }

    public function create()
    {
        return view('course/add_video', [
            'period_id' => input('period'),
            'course_id' => input('course'),
            ]);

    }

    public function save()
    {
        $input = input('post.');
        $video = new VideoModel;
        $video->period_id = $input['period_id'];
        $video->course_id = $input['course_id'];
        $video->video_src = $input['video_src'];
        if ($video->save()) {
            $this -> success('修改成功', "/admin/video?period={$input['period_id']}&course={$input['course_id']}");
        } else {
            $this -> error('添加失败');
        }
    }

    public function read($id)
    {
        $videoModel = VideoModel::where('period_id', $id)->find();
        return view('course/video', ['video' => $videoModel]);
    }

    public function edit($id)
    {
        // halt(VideoModel::get(['period_id' => $id,]));
        return view('course/edit_video', [
            'period_id' => $id,
            'course_id' => input('course'),
            'video' => VideoModel::get(['period_id' => $id,]),
        ]);

    }

    public function update($id)
    {
        $input = input('put.');
        $video = VideoModel::get(['period_id' => $id,]);
        $video->video_src = $input['video_src'];
        if ($video->save()) {
            $this -> success('修改成功', "/admin/video?period={$id}&course={$input['course_id']}");
        } else {
            $this -> error('修改失败');
        }

    }

    public function delete()
    {}
}

